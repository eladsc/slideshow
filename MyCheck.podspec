
Pod::Spec.new do |s|

  s.name         = "MyCheck Preview"
  s.version      = "0.0.1"
s.summary      = "MyCheck’s mobile payment technology transforms the guest experience with faster checkout, increased loyalty and customer engagement."

  s.homepage     = "http://info.mycheckapp.com"

s.license      = { :type => 'Apache License, Version 2.0', :text => <<-LICENSE
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
LICENSE
}

s.source_files  = 'MyCheck/**/*.{h,m}'


s.author    =   {"Michal Shatz" => "michal@msapps.mobi"}

s.resources    = "mycheckslide/*.xib", "mycheckslide/*.png", "mycheckslide/Base.lproj/*.storyboard"

s.source_files  = 'mycheckslide', 'mycheckslide/**/*.{h,m}'

s.public_header_files = 'MyCheck/**/*.h'

s.platform     = :ios, '7.0'

s.source = {:git => 'https://eladsc@bitbucket.org/eladsc/slideshow.git' , :tag => "0.0.1"}





s.frameworks =  'UIKit'

end
