//
//  MyCheck.m
//  MyCheck
//
//  Created by Michal Shatz on 3/12/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyCheck.h"
#import "MCViewController.h"


@interface MyCheck ()

@property (nonatomic, strong) NSString *deviceId;

@end

@implementation MyCheck

@synthesize locationId;

+(void)installForAppType:(NSString*)appType locale:(NSString*)locale deviceId:(NSString *)deviceId{
 
    [self installForAppType:appType locale:locale];

}

+(void)installForAppType:(NSString*)appType locale:(NSString*)locale{
   
}

+(instancetype)sharedInstance{
    static MyCheck *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[MyCheck alloc] init];
    });
    return _sharedClient;
}

-(void)showMyCheck:(UIViewController *)viewController{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"MCMain" bundle:nil];

    UINavigationController* controller = [story instantiateViewControllerWithIdentifier:@"first"];
    [viewController presentViewController:controller animated:YES completion:nil];
}

-(void)setStrings{
    
}




@end