//
//  MyCheck.h
//  MyCheck
//
//  Created by Michal Shatz on 3/12/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


/**
 * This document describes the API exposed by the MyCheck SDK (1.x) which the developers can use to integrate MyCheck support into their iOS applications. 
 */

//! Project version number for MyCheck.
FOUNDATION_EXPORT double MyCheckVersionNumber;

//! Project version string for MyCheck.
FOUNDATION_EXPORT const unsigned char MyCheckVersionString[];

@interface MyCheck : NSObject

/** Initialize helpshift support
 *
 * When initializing MyCheck you must pass these two tokens. You initialize MyCheck by adding the following lines in the implementation file for your app delegate, ideally at the top of application:didFinishLaunchingWithOptions. 
 *  @param appType This is your developer API Key
 *  @param locale This is your locale
 *
 *  @available Available in SDK version 4.0.0 or later
 */
+(void)installForAppType:(NSString*) appType locale:(NSString*)locale deviceId:(NSString *)deviceId;

/** Returns an instance of MyCheck
 *
 * When calling any MyCheck instance method you must use sharedInstance. 
 */
+(instancetype)sharedInstance;

/** Show the payment screen with all payment options
 *
 *
 * @param viewController viewController on which the MyCheck payment options screen will show up.
 */
-(void)showMyCheck:(UIViewController *)viewController;

/*
 The location id used to create the MyCheck share instance
 */
@property (nonatomic, strong) NSString * locationId;




@end

